module gitlab.com/iccfpga-rv/iccfpga-go-lib

go 1.13

require (
	github.com/iotaledger/iota.go v1.0.0-beta.14
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)
