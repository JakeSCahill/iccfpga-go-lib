package iccfpga

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io/ioutil"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/checksum"
	. "github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/signing"
	. "github.com/iotaledger/iota.go/trinary"
	"github.com/tarm/serial"
)

// ICCFPGAConfig defines an object that configures a connection to the CryptoCore.
type ICCFPGAConfig struct {
	// The serial port that the CryptoCore is using
	Serial string
	// The API key that's in the CryptoCore firmware
	ApiKey [48]byte
	// Whether to save the seed in RAM when it's first loaded from the secure element
	KeepSeedInRAM bool
	// Whether to print to the console the API command that was called
	// and how long the CryptoCore took to send a response
	Verbose bool
	// Whether to print debugging information to the console for each API call
	Debug bool
}

// ICCFPGA defines an object that can call API commands on the CryptoCore
type ICCFPGA struct {
	// The configuration object to use
	config ICCFPGAConfig
	// Any flags to use
	flags map[string]interface{}
	// The default configuration settings to use for serial communication
	comm *serial.Config
	port *serial.Port
}

// Flags defines the flags that are set in the CryptoCore
type Flags struct {
	KeepSeedInRAM bool
}

type ProgressFunc func(start int, stop int, progress int)

const (
	lenBitstreamBits uint32 = 17536096
)

func min(a int, b int) int {
	if a <= b {
		return a
	} else {
		return b
	}
}

// Init initializes a connection to the CryptoCore, using an ICCFPGAConfig object.
//
// Parameters
// - config: The ICCFPGAConfig object to use
//
// Returns
// An ICCFPGA object that you can use to call CryptoCore API commands.
func (iccfpga *ICCFPGA) Init(config ICCFPGAConfig) error {
	iccfpga.config = config
	iccfpga.flags = make(map[string]interface{})
	iccfpga.flags["keepSeedInRAM"] = config.KeepSeedInRAM

	iccfpga.comm = &serial.Config{Name: iccfpga.config.Serial, Baud: 115200}

	var err error
	iccfpga.port, err = serial.OpenPort(iccfpga.comm)
	if err != nil {
		return err
	}

	_, err = iccfpga.apiSetFlags(iccfpga.flags)
	if err != nil {
		return err
	}
	return nil
}

// GenerateAddresses generates addresses for the seed in given key of the secure element.
//
// Note: Before you can call this method, you must initalize the secure
// element by configuring the the ICCFPGA object with an ApiKey.
//
// Returns
// One or more 81-tryte addresses
func (iccfpga *ICCFPGA) GenerateAddresses(seed Trytes, index uint64, total uint64, secLvl SecurityLevel, addChecksum bool) ([]Hash, error) {
	addresses := make(Hashes, total)

	number := int(total)
	ofs := int(index)
	for number > 0 {
		chunk := min(number, 100)
		trytes, err := iccfpga.apiGenerateAddress(0, ofs, int(secLvl), chunk)
		if err != nil {
			return nil, err
		}
		for j := 0; j < chunk; j++ {
			if addChecksum {
				addresses[j], err = checksum.AddChecksum(trytes[j], true, 9)
				if err != nil {
					return nil, err
				}
			} else {
				addresses[j] = trytes[j]
			}
		}
		ofs += chunk
		number -= chunk
	}
	return addresses, nil
}

// SignInputs signs input transactions, using the seed in the given key of the secure element.
//
// Note: Before you can call this method, you must initalize the secure
// element by configuring the the ICCFPGA object with an ApiKey.
//
// Parameters
// - slot: The key of the seed in the secure element to use to sign the transactions
// - inputs: Input objects that each include an address that you want to withdraw IOTA tokens from,
// the security level of the address, and the key index of the address
// - bundleHash: The bundle hash to sign with the private keys of the given input addresses
//
// Returns
// An array of signatures in the same order as the given inputs
func (iccfpga *ICCFPGA) SignInputs(slot int, inputs []Input, bundleHash Trytes) ([]Trytes, error) {
	signedFrags := []Trytes{}
	for i := range inputs {
		input := &inputs[i]
		var sec SecurityLevel
		if input.Security == 0 {
			sec = SecurityLevelMedium
		} else {
			sec = input.Security
		}

		frags := make([]Trytes, input.Security)
		signedTrytes, err := iccfpga.apiSignTransaction(slot, uint32(input.KeyIndex), bundleHash, uint32(sec))
		if err != nil {
			return nil, err
		}
		// verify signature
		ok, err := signing.ValidateSignatures(input.Address[:81], signedTrytes, bundleHash)
		if err != nil || !ok {
			return nil, err
		}

		for i := 0; i < int(input.Security); i++ {
			frags[i] = signedTrytes[i]
		}

		signedFrags = append(signedFrags, frags...)
	}
	return signedFrags, nil
}

// PoWFunc does proof of work on a bundle's transaction trytes.
//
// This command can do proof of work for up to eight transactions at once.
//
// Parameters
// - trytes: Transaction trytes
// - mwm: The minimum weight magnitude to use during proof of work
//
// Returns
// Transaction trytes that include a valid nonce and attachment timestamps
func (iccfpga *ICCFPGA) PoWFunc(trytes Trytes, mwm int, parallelism ...int) (Trytes, error) {
	return iccfpga.apiDoPoW(trytes, mwm)
}

// ReadFlashToBytes reads 4 kB pages from the QSPI flash memory.
// The output data is in base64 format.
func (iccfpga *ICCFPGA) ReadFlashToBytes(pageOfs int, progress ProgressFunc) ([]byte, error) {
	bytesToRead := int(lenBitstreamBits / 8)
	pagesToRead := bytesToRead / 4096
	if bytesToRead%4096 != 0 {
		pagesToRead += 1
	}
	var core bytes.Buffer
	for page := 0 + pageOfs; page < pagesToRead+pageOfs; page++ {
		response, err := iccfpga.apiReadFlashPage(page)
		if err != nil {
			return nil, err
		}

		if err != nil {
			return nil, err
		}
		decoded, err := base64.StdEncoding.DecodeString(response["data"].(string))

		if len(decoded) != 4096 {
			return nil, errors.New("invalid decoded size")
		}
		_, err = core.Write(decoded)
		if err != nil {
			return nil, err
		}
		if progress != nil {
			progress(int(pageOfs), int(pagesToRead+pageOfs), int(page))
		}
	}
	return core.Bytes(), nil
}

// ReadFlashToFile reads 4 kB pages from the QSPI flash memory and saves it to a file.
// The output data is in base64 format.
func (iccfpga *ICCFPGA) ReadFlashToFile(filename string, pageOfs int, progress ProgressFunc) error {
	data, err := iccfpga.ReadFlashToBytes(pageOfs, progress)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0644)
	return err
}

// WriteFlashFromBytes writes one or more 4 kB pages to the QSPI flash memory.
// This way, the soft CPU can update the entire system by writing new bitstreams into flash.
//
// Note: Before you can call this method, you must initalize the secure
// element by configuring the the ICCFPGA object with an ApiKey.
func (iccfpga *ICCFPGA) WriteFlashFromBytes(pageOfs uint32, data []byte, progress ProgressFunc) error {
	numPages := uint32(len(data) / 4096)
	if len(data)%4096 != 0 {
		numPages += 1
	}
	var err error
	for i := pageOfs; i < numPages+pageOfs; i++ {
		encoded := base64.StdEncoding.EncodeToString(data[(i-pageOfs)*4096 : (i+1-pageOfs)*4096])
		_, err = iccfpga.apiWriteFlashPage(i, encoded)
		if err != nil {
			return err
		}
		if progress != nil {
			progress(int(pageOfs), int(numPages+pageOfs), int(i))
		}
	}
	return err
}

// WriteFlashFromFile writes one or more 4 kB pages from a file to the QSPI flash memory.
// This way, the soft CPU can update the entire system by writing new bitstreams into flash.
//
// Note: Before you can call this method, you must initalize the secure
// element by configuring the the ICCFPGA object with an ApiKey.
func (iccfpga *ICCFPGA) WriteFlashFromFile(filename string, pageOfs uint32, progress ProgressFunc) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return iccfpga.WriteFlashFromBytes(pageOfs, data, progress)
}

// GetVersion gets the current version of the firmware that the CryptoCore is running.
func (iccfpga *ICCFPGA) GetVersion() (string, error) {
	return iccfpga.apiGetVersion()
}
