package iccfpga

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/iotaledger/iota.go/kerl"
	keccak "github.com/iotaledger/iota.go/kerl/sha3"
	"github.com/iotaledger/iota.go/trinary"
	. "github.com/iotaledger/iota.go/trinary"
)

// care for endianess!
func u32ToBytes(value uint32) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, value)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

func (iccfpga *ICCFPGA) apiCall(cmd map[string]interface{}) (map[string]interface{}, error) {
	//time.Sleep(500 * time.Millisecond)
	bytes, err := json.Marshal(cmd)
	bytes = append(bytes, '\n')

	if iccfpga.config.Debug {
		fmt.Printf("\n------------\n")
		fmt.Printf("-> %s\n", string(bytes))
	}
	written, err := iccfpga.port.Write(bytes)
	if err != nil {
		return nil, errors.New("error writing data")
	}

	if written != len(bytes) {
		return nil, errors.New("not all bytes written")
	}

	reader := bufio.NewReader(iccfpga.port)
	reply, err := reader.ReadBytes('\x0a') // read until \n
	if err != nil {
		return nil, err
	}

	if iccfpga.config.Debug {
		fmt.Printf("<- %s\n", string([]byte(reply)))
	}

	var response map[string]interface{}
	err = json.Unmarshal([]byte(reply), &response)
	if err != nil {
		return nil, err
	}

	if int(response["code"].(float64)) != 200 { // something went wrong ...
		return nil, errors.New(response["error"].(string))
	}

	if iccfpga.config.Verbose {
		fmt.Printf("%s: %dms\n", response["command"], int(response["duration"].(float64)))
	}

	return response, nil
}

/*
func TestX() {
	key := 123456789
	addressIndex := uint32(987654321)
	bundleHash := "ACRSXEDHISHHDKWJITDCGLEPQQTNWIAYHSZASCUEEGQADTKJONSCFTU9FQ9GLYJLYCSIUXKUWLFQRECID"
	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(uint32(key)))
	sha.Write(u32ToBytes(addressIndex))
	sha.Write([]byte(string(bundleHash)))
	sha.Write(apiKey[:])
	trits, err := kerl.KerlBytesToTrits(sha.Sum(nil))
	if err != nil {
		return
	}

	apiHash, err := trinary.TritsToTrytes(trits)
	if err != nil {
		return
	}
	fmt.Println(apiHash)
}
*/
// SignTransaction signs a transaction
func (iccfpga *ICCFPGA) apiSignTransaction(key int, addressIndex uint32, bundleHash Trytes, securityLevel uint32) ([]Trytes, error) {
	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(uint32(key)))
	sha.Write(u32ToBytes(addressIndex))
	sha.Write([]byte(string(bundleHash)))
	sha.Write(iccfpga.config.ApiKey[:])
	trits, err := kerl.KerlBytesToTrits(sha.Sum(nil))
	if err != nil {
		return nil, err
	}

	apiHash, err := trinary.TritsToTrytes(trits)
	if err != nil {
		return nil, err
	}

	cmd := make(map[string]interface{})
	cmd["command"] = "signTransaction"
	cmd["key"] = key
	cmd["addressIndex"] = addressIndex
	cmd["bundleHash"] = string(bundleHash)
	cmd["securityLevel"] = securityLevel
	cmd["auth"] = string(apiHash)

	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		slice := Trytes(trytes.(string))
		list = append(list, slice[0:2187])
	}
	return list, nil
}

// GenerateRandomSeed generates a random seed and
// stores it in one of eight available memory addresses in the secure element.
//
// Note: Before you can call this method, you must initalize the secure
// element by configuring the the ICCFPGA object with an APIKey.
func (iccfpga *ICCFPGA) GenerateRandomSeed(key int) error {
	return iccfpga.apiGenerateRandomSeed(key)
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiGenerateRandomSeed(key int) error {
	cmd := make(map[string]interface{})
	cmd["command"] = "generateRandomSeed"
	cmd["key"] = key
	_, err := iccfpga.apiCall(cmd)
	if err != nil {
		return err
	}

	return nil
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiGetVersion() (string, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "version"
	resp, err := iccfpga.apiCall(cmd)

	if err != nil {
		return "", err
	}
	return resp["version"].(string), nil
}

// GenerateAddress generate Address on FPGA
func (iccfpga *ICCFPGA) apiGenerateAddress(key int, index int, security int, number int) ([]Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "generateAddress"
	cmd["firstIndex"] = index
	cmd["number"] = number
	cmd["security"] = security
	cmd["key"] = key
	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		list = append(list, Trytes(trytes.(string)))
	}
	return list, nil
}

// DoPoW do PoW
func (iccfpga *ICCFPGA) apiDoPoW(trytes Trytes, mwm int) (Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "doPow"
	cmd["trytes"] = [1]string{trytes}
	cmd["minWeightMagnitude"] = mwm
	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return "", err
	}

	responseTrytes := response["trytes"].([]interface{})
	return Trytes(responseTrytes[0].(string)), nil
}

func (iccfpga *ICCFPGA) apiSetFlags(flags map[string]interface{}) (map[string]interface{}, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "setFlags"
	cmd["flags"] = flags
	return iccfpga.apiCall(cmd)
}

func (iccfpga *ICCFPGA) apiReadFlashPage(page int) (map[string]interface{}, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "readFlashPage"
	cmd["page"] = page
	return iccfpga.apiCall(cmd)
}

func (iccfpga *ICCFPGA) apiWriteFlashPage(page uint32, data string) (map[string]interface{}, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "writeFlashPage"
	cmd["page"] = page
	cmd["data"] = data

	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(page))
	sha.Write([]byte(data))
	sha.Write(iccfpga.config.ApiKey[:])
	trits, err := kerl.KerlBytesToTrits(sha.Sum(nil))
	if err != nil {
		return nil, err
	}

	apiHash, err := trinary.TritsToTrytes(trits)
	if err != nil {
		return nil, err
	}
	cmd["auth"] = apiHash
	//	fmt.Printf("%s\n", data)
	return iccfpga.apiCall(cmd)
}
